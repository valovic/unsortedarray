﻿using System;

namespace UnsortedArray
{
    class Program
    {
        static void Main(string[] args)
        {
            uint[] unsortedArray = new uint[6] { 1, 2, 4, 34, 2, 1 };
            ISortingArrayMethods sortingArrayMethods = new SortingArrayMethods(unsortedArray);
            var result = sortingArrayMethods.FindUnsortedArray();

            Console.WriteLine($"{result.Item1} {result.Item2}");
        }
    }
}
