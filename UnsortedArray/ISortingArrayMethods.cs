﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnsortedArray
{
    public interface ISortingArrayMethods
    {
        public Tuple<uint, uint> FindUnsortedArray();
    }
}
