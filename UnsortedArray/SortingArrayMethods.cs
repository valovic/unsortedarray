﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnsortedArray
{
    public class SortingArrayMethods : ISortingArrayMethods
    {
        private readonly uint[] _unsortedArray;
        private uint LeftPointer;
        private uint RightPointer;
        private uint subArrayMaximumValue;
        private uint subArrayMinValue;
        private int StopCounter = 0;

        public SortingArrayMethods(uint[] unsortedArray)
        {
            _unsortedArray = unsortedArray;
        }
        public Tuple<uint, uint> FindUnsortedArray()
        {
            Tuple<uint, uint> unsortedArrayIndexes = null;

            unsortedArrayIndexes = CheckBoundaryCondition();
            if (unsortedArrayIndexes == null)
            {
                CheckArrayForIncreasing();
                unsortedArrayIndexes = CheckStopCondition();
            }

            return unsortedArrayIndexes;
        }

        private Tuple<uint, uint> CheckBoundaryCondition()
        {
            Tuple<uint, uint> unsortedArrayIndexes = null;

            if (_unsortedArray.Length == 0 || _unsortedArray.Length == 1 || _unsortedArray.Length == 2)
            {
                unsortedArrayIndexes = new Tuple<uint, uint>(0, 0);
            }

            return unsortedArrayIndexes;
        }

        private void CheckArrayForIncreasing()
        {
            if (_unsortedArray.Length > 2)
            {
                uint middleIndex = (uint)_unsortedArray.Length / 2;

                LeftPointer = middleIndex;
                RightPointer = middleIndex;
                subArrayMaximumValue = _unsortedArray[middleIndex];
                subArrayMinValue = _unsortedArray[middleIndex];

                while (StopCounter < 2)
                {
                    CheckLeftValue();

                    if (StopCounter == 2)
                    {
                        break;
                    }

                    CheckRightValue();
                }
            }
        }

        private Tuple<uint, uint> CheckStopCondition()
        {
            Tuple<uint, uint> unsortedArrayIndexes = null;

            if (LeftPointer == RightPointer)
            {
                unsortedArrayIndexes = new Tuple<uint, uint>(0, 0);
            }
            else
            {
                unsortedArrayIndexes = new Tuple<uint, uint>(LeftPointer, RightPointer);
            }

            return unsortedArrayIndexes;
        }

        private void CheckLeftValue()
        {
            if (LeftPointer != 0)
            {
                MoveToTheLeft();
            }
            else
            {
                StopCounter++;
            }
        }

        private void CheckRightValue()
        {
            if (RightPointer != _unsortedArray.Length - 1)
            {
                MoveToTheRight();
            }
            else
            {
                StopCounter++;
            }
        }

        private void MoveToTheLeft()
        {
            var leftValue = _unsortedArray[LeftPointer - 1];
            var prevLeftValue = _unsortedArray[LeftPointer];

            if (leftValue > prevLeftValue || leftValue > subArrayMinValue)
            {
                if (leftValue > subArrayMaximumValue)
                {
                    subArrayMaximumValue = leftValue;
                }
                LeftPointer--;
                StopCounter = 0;
            }
            else
            {
                StopCounter++;
            }
        }

        private void MoveToTheRight()
        {
            var rightValue = _unsortedArray[RightPointer + 1];
            var prevRightValue = _unsortedArray[RightPointer];

            if ((rightValue < prevRightValue || rightValue < subArrayMaximumValue))
            {
                if (rightValue < subArrayMinValue)
                {
                    subArrayMinValue = rightValue;
                }
                RightPointer++;
                StopCounter = 0;
            }
            else
            {
                StopCounter++;
            }
        }
    }
}
