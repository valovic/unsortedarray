using Xunit;
using UnsortedArray;
using System;

namespace TestUnsortedArray
{
    public class TestsUnsortedArrayMethods
    {
        [Theory]
        [InlineData(new uint[0] { })]
        [InlineData(new uint[1] { 1 })]
        [InlineData(new uint[2] { 1, 2 })]
        [InlineData(new uint[2] { 2, 1 })]
        public void ArgumentArray_HasBorderValues(uint[] unsortedArray)
        {
            ISortingArrayMethods sortingArrayMethods = new SortingArrayMethods(unsortedArray);
            var result = sortingArrayMethods.FindUnsortedArray();

            Assert.True(result.Equals(new Tuple<uint, uint>(0, 0)));
        }

        [Fact]
        public void ArgumentArray_HasManyValues1()
        {
            uint[] unsortedArray = new uint[6] { 1, 4, 3, 2, 3, 4 };
            ISortingArrayMethods sortingArrayMethods = new SortingArrayMethods(unsortedArray);
            var result = sortingArrayMethods.FindUnsortedArray();

            Assert.True(result.Equals(new Tuple<uint, uint>(1, 4)));
        }

        [Fact]
        public void ArgumentArray_HasManyValues2()
        {
            uint[] unsortedArray = new uint[6] { 10, 4, 3, 2, 3, 4 };
            ISortingArrayMethods sortingArrayMethods = new SortingArrayMethods(unsortedArray);
            var result = sortingArrayMethods.FindUnsortedArray();

            Assert.True(result.Equals(new Tuple<uint, uint>(0, 5)));
        }

        [Fact]
        public void ArgumentArray_HasManyValues3()
        {
            uint[] unsortedArray = new uint[6] { 1, 2, 3, 8, 7, 5 };
            ISortingArrayMethods sortingArrayMethods = new SortingArrayMethods(unsortedArray);
            var result = sortingArrayMethods.FindUnsortedArray();

            Assert.True(result.Equals(new Tuple<uint, uint>(3, 5)));
        }

        [Fact]
        public void ArgumentArray_HasManyValues4()
        {
            uint[] unsortedArray = new uint[7] { 10, 5, 7, 1, 2, 3, 5 };
            ISortingArrayMethods sortingArrayMethods = new SortingArrayMethods(unsortedArray);
            var result = sortingArrayMethods.FindUnsortedArray();

            Assert.True(result.Equals(new Tuple<uint, uint>(0, 6)));
        }

        [Fact]
        public void ArgumentArray_HasManyValues_InTheMiddleMax()
        {
            uint[] unsortedArray = new uint[6] { 1, 2, 3, 10, 6, 7 };
            ISortingArrayMethods sortingArrayMethods = new SortingArrayMethods(unsortedArray);
            var result = sortingArrayMethods.FindUnsortedArray();

            Assert.True(result.Equals(new Tuple<uint, uint>(3, 5)));
        }

        [Fact]
        public void ArgumentArray_HasManyValues_InTheMiddleMin()
        {
            uint[] unsortedArray = new uint[6] { 2, 3, 4, 1, 6, 7 };
            ISortingArrayMethods sortingArrayMethods = new SortingArrayMethods(unsortedArray);
            var result = sortingArrayMethods.FindUnsortedArray();

            Assert.True(result.Equals(new Tuple<uint, uint>(0, 3)));
        }

    }
}